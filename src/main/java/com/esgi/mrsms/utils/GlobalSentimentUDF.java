package com.esgi.mrsms.utils;

import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF4;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import javax.xml.crypto.Data;
import java.io.File;
import java.util.List;

public class GlobalSentimentUDF implements UDF4<String, String, String, String, Integer> {

    private SparkSession session;
    private StructType metadata;
    private Dataset<Row> rows;

    private static final String srcPathCaro = "/Users/caroline/Desktop/WordWizard/src/main/resources/word_sentiment.csv";
    private static final String srcPathRV = "/home/tantepata/Bureau/BigData/WordWizard/src/main/resources/word_sentiment.csv";

    private static final String srcPath = srcPathRV;

    public GlobalSentimentUDF(SparkSession session) throws AnalysisException {
        super();
        this.session = session;

        assert new File(srcPath).exists();
        this.metadata = new StructType(new StructField[]{
                new StructField("word", DataTypes.StringType, true, Metadata.empty()),
                new StructField("sentiment", DataTypes.IntegerType, true, Metadata.empty()),
        });

        this.rows = session.read()
                .option("mode","PERMISSIVE")
                .option("header", true)
                .option("delimiter", ",")
                .schema(metadata)
                .csv(srcPath);
        rows.createTempView("RAW");
    }

    @Override
    public Integer call(String s, String s2, String s3, String s4) {

        String[] words = {s, s2, s3, s4};
        StringBuilder listWords = new StringBuilder("(");
        for (String w : words) {
            if (w != null) {
                listWords.append("'").append(w).append("'").append(",");
            } else {
                listWords.append("@").append(","); // supposed to match nothing
            }
        }
        listWords.deleteCharAt(listWords.length() - 1).append(")");
        System.out.println(listWords.toString());
        Dataset<Row> results = rows.where("word in " + listWords.toString());

        List<Row> array = results.collectAsList();
        Integer sum = 0;
        for (Row r : array) {
            // System.out.println("word : " + r.getString(0) + " sentiment : " + r.getInt(1));
            sum += r.getInt(1);
        }

        return sum;
    }
}
