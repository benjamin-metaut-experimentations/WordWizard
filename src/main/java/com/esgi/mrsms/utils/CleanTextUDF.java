package com.esgi.mrsms.utils;

import org.apache.spark.sql.api.java.UDF1;

public class CleanTextUDF implements UDF1<String, String> {

    @Override
    public String call(String tweet) throws Exception {
        return tweet.replace(".", "")
                .replace(",","")
                .replace("'", "")
                .replace("?", "")
                .replace("!", "")
                .trim();
    }

}