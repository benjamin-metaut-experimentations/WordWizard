package com.esgi.mrsms.builder;

import com.esgi.mrsms.utils.CorpusReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ThreeGram {

    public void run(String source, SparkSession session, String outputPath) throws Exception {
        // Typer ce qu'on va lire
        StructField[] structFields = new StructField[]{
                new StructField("col1", DataTypes.StringType, true, Metadata.empty()),
                new StructField("col2", DataTypes.StringType, true, Metadata.empty()),
                new StructField("col3", DataTypes.StringType, true, Metadata.empty()),
                new StructField("col4", DataTypes.StringType, true, Metadata.empty())
        };
        StructType structType = new StructType(structFields);

        // ajouter les triplets de mots dans l'ordre
        List<Row> rows = new ArrayList<>();
        try (CorpusReader reader = new CorpusReader(source)) {
            Iterator<String> iterator = reader.iterator();

            String current = iterator.next();
            String next = iterator.next();
            String nextNext = iterator.next();
            String nextNextNext = iterator.next();

            rows.add(RowFactory.create(current, next, nextNext, nextNextNext));
            while (iterator.hasNext()) {
                current = next;
                next = nextNext;
                nextNext = nextNextNext;
                nextNextNext = iterator.next();
                rows.add(RowFactory.create(current, next, nextNext, nextNextNext));
            }
        }

        Dataset<Row> allTriplets = session.createDataFrame(rows, structType);
        allTriplets.createTempView("QUADRU");
        Dataset<Row> countedTriplets = allTriplets.sqlContext().sql("select col1, col2, col3, col4, count(*) as COUNTER"
                + " from QUADRU group by col1, col2,  col3, col4"
                + " HAVING COUNTER > 100 "
                + " order by COUNTER desc, col1 asc, col2 asc, col3 asc");

        countedTriplets.repartition(1).write().option("header", true).option("delimiter", ",").csv(outputPath);
    }
}