package com.esgi.mrsms;

import org.apache.commons.lang.NullArgumentException;
import org.apache.spark.sql.*;

import java.util.List;

public class Wizard {

    public static Object[] suggestForOne(SparkSession session, String firstWord, Integer nbSuggestions) throws NullArgumentException, AnalysisException {
        if (session == null) {
            throw new NullArgumentException("session can't be null");
        }
        if (firstWord == null) {
            throw new NullArgumentException("First word can't be null");
        }
        if (nbSuggestions == null) {
            nbSuggestions = 10;
        }

        String path = "/Users/caroline/Desktop/esgi_201904/bdt/mrsms/src/main/resources/basic_recommendations.txt";

        Dataset<Row> rows = session.read()
                .option("header", true)
                .option("inferSchema", true)
                .option("delimiter", ";")
                .csv(path);

        rows.createTempView("MATCHES");
        List<Row> results = session.sql("select col2, counter from MATCHES "
                + "where col1 = '" + firstWord + "' "
                + "order by counter desc "
                + "LIMIT " + nbSuggestions
        ).collectAsList();

        return results.toArray();
    }

    public static Object[] suggestForTwo(SparkSession session, String firstWord, String secondWord, Integer nbSuggestions) throws NullArgumentException, AnalysisException {
        if (session == null) {
            throw new NullArgumentException("session can't be null");
        }
        if (firstWord == null) {
            throw new NullArgumentException("First word can't be null");
        }
        if (secondWord == null) {
            throw new NullArgumentException("Second word can't be null");
        }
        if (nbSuggestions == null) {
            nbSuggestions = 10;
        }

        String path = "/Users/caroline/Desktop/esgi_201904/bdt/mrsms/src/main/resources/advanced.txt";

        Dataset<Row> rows = session.read()
                .option("header", true)
                .option("inferSchema", true)
                .option("delimiter", ";")
                .csv(path);

        rows.createTempView("CPX_MATCHES");
        List<Row> results = session.sql("select col3, counter from CPX_MATCHES "
                + "where col1 = '" + firstWord + "' "
                + "and col2 = '" + secondWord + "' "
                + "order by counter desc "
                + "LIMIT " + nbSuggestions
        ).collectAsList();

        return results.toArray();
    }
}