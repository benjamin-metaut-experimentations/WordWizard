package com.esgi.mrsms;

import com.esgi.mrsms.builder.OneGram;
import com.esgi.mrsms.builder.ThreeGram;
import com.esgi.mrsms.builder.TwoGram;
import com.esgi.mrsms.builder.ZeroGram;
import com.esgi.mrsms.utils.GlobalSentimentUDF;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.util.Date;

public class Main {

    private static final String BasePathRV = "/Users/tantepata/Google Drive/Cour/5A MOC/Semestre 2/Big Data/WordWizard";
    private static final String BasePathRVLinux = "/home/tantepata/Bureau/BigData/WordWizard";
    private static final String BasePathCaro = "/Users/caroline/Desktop/WordWizard";

    public static String source;

    public static void main(String[] args) throws Exception {
        Date debut = new Date();
        Date fin = new Date();

        // gestion des logs
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        //final String CurrentBasePath = BasePathCaro;
        final String CurrentBasePath = BasePathRVLinux;

        final String sourceProd = CurrentBasePath + "/src/main/resources/bigExample.txt";
        final String sourceTest = CurrentBasePath + "/src/main/resources/example.txt";

        source = sourceTest;

        final String output0G = CurrentBasePath + "/src/main/resources/0G.txt";
        final String output0Gmini = CurrentBasePath + "/src/main/resources/mini0G.txt";

        final String output1G = CurrentBasePath + "/src/main/resources/1G.txt";
        final String output1Gmini = CurrentBasePath + "/src/main/resources/mini1G.txt";

        final String output2G = CurrentBasePath + "/src/main/resources/2G.txt";
        final String output2Gmini = CurrentBasePath + "/src/main/resources/mini2G.txt";

        final String output3G = CurrentBasePath + "/src/main/resources/3G.txt";
        final String output3Gmini = CurrentBasePath + "/src/main/resources/mini3G.txt";

        // les éléments pour faire du spark sql
        System.setProperty("spark.cores.max", "5");
        System.setProperty("spark.executor.memory", "10G");

        SparkConf configuration = new SparkConf().setAppName("WordReader").setMaster("local[5]");
        try (SparkSession session = SparkSession.builder().config(configuration).getOrCreate()) {
            //construction des data
            //new ZeroGram().run(source, session, output0Gmini);
            fin = new Date();
            System.out.println("0G: " + debut.toString() + " - " + fin.toString());
            debut = new Date();

            new OneGram().run(source, session, output1Gmini);
            fin = new Date();
            System.out.println("1G: " + debut.toString() + " - " + fin.toString());
            debut = new Date();

            //new TwoGram().run(source, session, output2G);
            fin = new Date();
            System.out.println("2G: " + debut.toString() + " - " + fin.toString());
            debut = new Date();

            //new ThreeGram().run(source, session, output3G);
            fin = new Date();
            System.out.println("3G: " + debut.toString() + " - " + fin.toString());

            //new SentimentMeter().train(sentimentTrainingDataPath, session, outputSentimentPath);
            /*
            GlobalSentimentUDF udf = new GlobalSentimentUDF(session);
            Integer res = udf.call("rouge", "bleu", "vert", "jaune");
            System.out.println(res);
            res = udf.call("joyeux", "gentil", "beau", "gai");
            System.out.println(res);
            res = udf.call("triste", "odieux", "laid", "fade");
            System.out.println(res);
			/*
			System.out.println("With just 'je'");
            Object[] suggestedWords = Wizard.suggestForOne(session, "je", 10);
            displayList(suggestedWords);
            System.out.println("With 'alors'  'je'");
            suggestedWords = Wizard.suggestForTwo(session, "alors", "je", 10);
            displayList(suggestedWords);

			 */
        }



    }

    public static void displayList(Object[] list) {
        for (Object elem : list) {
            System.out.println(elem.toString());
        }
    }
}