package com.esgi.mrsms.builder;

import java.util.LinkedList;
import java.util.List;

import com.esgi.mrsms.utils.CorpusReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

/**
 *
 */
public class ZeroGram {

    public void run(String source , SparkSession session, String outputPath) throws Exception {

        List<String> allWords = new LinkedList<>();
        
        try (CorpusReader reader = new CorpusReader(source)) {
            for (String element : reader) {
                allWords.add(element);
            }
        }
        Dataset<String> values = session.createDataset(allWords, Encoders.STRING());
        //long count = values.count();
        //System.out.println("Counter : \t\t" + count);
        values.createTempView("CORPUS");
        session.sql("select value, count(*) as COUNTER from CORPUS "
                + " group by value  HAVING COUNTER > 100 order by COUNTER DESC ")
                .repartition(1)
                .write()
                .option("header", true)
                .option("delimiter", ",")
                .csv(outputPath);

    }

}
